var map;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: { lat: -5.204869, lng: -37.315709 },
        disableDefaultUI: true
    });
    var princiCoords = [
        new google.maps.LatLng(-22.878735, -47.058499),
        new google.maps.LatLng(-22.879694, -47.058392),
        new google.maps.LatLng(-22.878686, -47.057598)
    ];
    var princiPolygon = new google.maps.Polygon({
        // Coordenadas do seu objeto
        paths: outerCoords,
        // Cor do da linha
        strokeColor: '#E12D93',

    });
    princiPolygon.setMap(map);
    var outerCoords = [
        new google.maps.LatLng(-5.204869, -37.315709),
        new google.maps.LatLng(-5.204842, -37.316047),
    ];
    var tCoords = [

        {
            lat: -5.204869,
            lng: -37.315709
        }, {
            lat: -5.204935,
            lng: -37.315713
        },
    ];

    map.data.add({
        geometry: new google.maps.Data.Polygon([outerCoords])
    });
    map.data.add({
        geometry: new google.maps.Data.Polygon([tCoords])
    });

    map.data.loadGeoJson('map.geojson');
}


window.eqfeed_callback = function(results) {
    for (var i = 0; i < results.features.length; i++) {
        var coords = results.features[i].geometry.coordinates;
        var latLng = new google.maps.LatLng(coords[1], coords[0]);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
    }
}
      function initMap() {
          var map = new google.maps.Map(document.getElementById('map'), {
              center: { lat: -34.397, lng: 150.644 },
              zoom: 19
          });
          var infoWindow = new google.maps.InfoWindow({ map: map });


          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {

                  var pos = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude

                  };

                  infoWindow.setPosition(pos);
                  infoWindow.setContent('Cadastrar esse local?');
                  map.setCenter(pos);
              }, function() {
                  handleLocationError(true, infoWindow, map.getCenter());
              });
          } else {

              handleLocationError(false, infoWindow, map.getCenter());
          }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
          infoWindow.setPosition(pos);
          infoWindow.setContent(browserHasGeolocation ?
              'Error: The Geolocation service failed.' :
              'Error: Your browser doesn\'t support geolocation.');
      }

      function localizar() {
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(showPosition);
          } else {
              alert("O seu navegador não suporta Geolocalização.");
          }

      }

      function showPosition(position) {
          document.getElementById("latitude").value = position.coords.latitude;
          document.getElementById("longitude").value = position.coords.longitude;
      }
function initMap() {
    var center = new google.maps.LatLng(-34.397, 150.644)
    var map = new google.maps.Map(document.getElementById('map'), {
        center: center,
        zoom: 19
    });
    var request = {
        location: center,
        radius: 8047,
        types: ['privado']
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, callback);
}

function callback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
        }
    }
}

function createMarker(place) {
    var placeloc = place.geometry.location;
    var marker = new google.maps.Marker({
        map: MutationObserver,
        position: place.geometry.location
    });
}
google.maps.event.addDomListener(window, 'load', initMap);
var express = require("express");
var bodyParser = require("body-parser");
var joi = require("joi");
var consign = require("consign");
const database = require("./database")();

module.exports = function () {
   var app = express();
   app.use(express.static('./public'));
   app.set("database",database);
   app.set("joi",joi);

   app.use(bodyParser.urlencoded({extended : true}));
   app.use(bodyParser.json());

   consign({cwd : "app"}).include("model").then("controller").then("route").into(app);
   return app;
}

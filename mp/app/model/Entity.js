module.exports = function (app) {
   var joi = app.get("joi");

   const schema = {
     name : joi.string().required(),
     hasProvidedServices: joi.array().items(joi.string()),
     hasPeopleExecutors: joi.array().items(joi.string()),
     hasBasedObjectGIS:joi.string(),
     properties:joi.object(),
     propertiesSource:joi.object(),
     _tags: joi.array().items(joi.string()),
     _super:joi.string()

   }

   return schema;
}

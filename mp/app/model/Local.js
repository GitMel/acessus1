module.exports = function (app) {
   var joi = app.get("joi");

   const schema = {
     nameLocal : joi.string().required(),
     latitude : joi.string().required(),
     longitude : joi.string().required(),

   }

   return schema;
}

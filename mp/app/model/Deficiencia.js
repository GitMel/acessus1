module.exports = function (app) {
   var joi = app.get("joi");

   const schema = {
     name : joi.string().required(),
   }

   return schema;
}

module.exports = function (app) {
   var joi = app.get("joi");

   const schema = {
     nameAdaptacao : joi.string().required(),
     hasAmbiente: joi.string()
   }

   return schema;
}

module.exports = function (app) {
   var joi = app.get("joi");

   const schema = {
     nameAdaptacao : joi.string().required(),
     forAmbiente: joi.string(),
     forDeficiencia: joi.string()
   }

   return schema;
}

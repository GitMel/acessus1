module.exports = function (app) {
    var model = app.model.Local;
    var joi = app.get("joi");
    var db = app.get("database");
    var dbLocal = db.collection("Local");


    var local = {};

       local.new = function (req,res) {
           var data = req.body;
           var validate = joi.validate(data,model);

           if(validate.error!=null) {
             res.status(400).json(validate.error);
           }
           else {

             dbLocal.save(data)
              .then(val => {

              },err => {
                res.status(500).json(err).end()
              })
           }
        };



    local.list = function (req,res) {
      dbLocal.all()
      .then(cursor => {
         cursor.all()
         .then(val => {
            res.status(200).json(val).end()
         })
      })
    };
 /*local.newgeojson = function (req,res) {
    var geojson = {"type":"Feature", "geometry":{"type":"Point","coordinates":[]}};
    var listgeojson = {};
      dbLocal.all()
      .then(cursor => {
         cursor.all()
         .then(val=> {
            val.longitude

            res.status(200).json(val).end()
         })
      })
    };*/
    local.lookup = function (req,res) {
      var id = req.params.id;
      dbLocal.document(id)
      .then(val => {
         res.status(200).json(val).end()
      }, err => {
         res.status(500).json(err).end()
      })
    };

    local.edit = function (req,res) {
      var id = req.params.id;
      var data = req.body;
      var validate = Joi.validate(data,model);
      if (result.error!=null) {
        res.status(400).json(validate.error);
      }
      else {
        dbLocal.update(id,data)
        .then(val => {
           res.status(200).json(val).end()
        }, err => {
           res.status(500).json(err).end()
        })
      }
    };

    local.delete = function (req,res) {
      var id = req.params.id;
      dbLocal.remove(id)
      .then(val => {
         res.status(200).json(val).end()
      }, err => {
         res.status(500).json(err).end()
      })
    }

    return local;
}

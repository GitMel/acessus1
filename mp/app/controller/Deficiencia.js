module.exports = function (app) {
    var model = app.model.Deficiencia;
    var joi = app.get("joi");
    var db = app.get("database");
    var dbEntity = db.collection("deficiencias");

    var entity = {};

    entity.new = function (req,res) {
       var data = req.body;
       var validate = joi.validate(data,model);

       if(validate.error!=null) {
         res.status(400).json(validate.error);
       }
       else {
          dbEntity.save(data)
          .then(val => {
             res.status(201).json(val).end()
          }, err => {
             res.status(500).json(err).end()
          })
       }
    };

    entity.list = function (req,res) {
      dbEntity.all()
      .then(cursor => {
         cursor.all()
         .then(val => {
            res.status(200).json(val).end()
         })
      })
    };

    entity.lookup = function (req,res) {
      var id = req.params.id;
      dbEntity.document(id)
      .then(val => {
         res.status(200).json(val).end()
      }, err => {
         res.status(500).json(err).end()
      })
    };

    entity.edit = function (req,res) {
      var id = req.params.id;
      var data = req.body;
      var validate = Joi.validate(data,model);
      if (result.error!=null) {
        res.status(400).json(validate.error);
      }
      else {
        dbEntity.update(id,data)
        .then(val => {
           res.status(200).json(val).end()
        }, err => {
           res.status(500).json(err).end()
        })
      }
    };

    entity.delete = function (req,res) {
      var id = req.params.id;
      dbEntity.remove(id)
      .then(val => {
         res.status(200).json(val).end()
      }, err => {
         res.status(500).json(err).end()
      })
    }

    return entity;
}

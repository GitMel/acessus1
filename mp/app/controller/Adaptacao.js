module.exports = function (app) {
    var model = app.model.Adaptacao;
    var joi = app.get("joi");
    var db = app.get("database");
    var dbAdaptacao = db.collection("Adaptacao");
    var forAmbiente = db.collection("forAmbiente");
    var forDeficiencia = db.collection("forDeficiencia");

    var adaptacao = {};

    adaptacao.new = function (req,res) {
       var data = req.body;
       var validate = joi.validate(data,model);

       if(validate.error!=null) {
         res.status(400).json(validate.error);
       }
       else {
         let _to = data.forAmbiente;
         let _tos = data.forDeficiencia;
         delete data.forAmbiente;
         delete data.forDeficiencia;
         dbAdaptacao.save(data)
          .then(val => {
            newForAmbiente(res,val._id,_to);
            newForDeficiencia(res,val._id,_tos);
          },err => {
            res.status(500).json(err).end()
          })
       }
    };
    async function newForDeficiencia(res,from,to) {
            var resultado = await forDeficiencia.save({_from:from,_to:to});
            res.status(200).json(resultado).end()
        }
    async function newForAmbiente(res,from,to) {
        var resultados = await forAmbiente.save({_from:from,_to:to});
        res.status(200).json(resultados).end()
    }

    adaptacao.list = function (req,res) {
      dbAdaptacao.all()
      .then(cursor => {
         cursor.all()
         .then(val => {
            res.status(200).json(val).end()
         })
      })
    };

    adaptacao.lookup = function (req,res) {
      var id = req.params.id;
      dbAdaptacao.document(id)
      .then(val => {
         res.status(200).json(val).end()
      }, err => {
         res.status(500).json(err).end()
      })
    };

    adaptacao.edit = function (req,res) {
      var id = req.params.id;
      var data = req.body;
      var validate = Joi.validate(data,model);
      if (result.error!=null) {
        res.status(400).json(validate.error);
      }
      else {
        dbAdaptacao.update(id,data)
        .then(val => {
           res.status(200).json(val).end()
        }, err => {
           res.status(500).json(err).end()
        })
      }
    };

    adaptacao.delete = function (req,res) {
      var id = req.params.id;
      dbAdaptacao.remove(id)
      .then(val => {
         res.status(200).json(val).end()
      }, err => {
         res.status(500).json(err).end()
      })
    }

    return adaptacao;
}

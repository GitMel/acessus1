module.exports = function(app) {
    var model = app.model.Ambiente;
    var joi = app.get("joi");
    var db = app.get("database");
    var dbAmbiente = db.collection("Ambiente");
    var hasLocalAmbiente = db.collection("hasLocalAmbiente");

    var ambiente = {};

    ambiente.new = function(req, res) {
        var data = req.body;

        if (validate.error != null) {
            res.status(400).json(validate.error);
        } else {
            let _to = data.hasLocalAmbiente;
            delete data.hasLocalAmbiente;
            dbAmbiente.save(data)
                .then(val => {
                    newHasLocalAmbiente(res, val._id, _to);

                }, err => {
                    res.status(500).json(err).end()
                })
        }
    };

    async function newHasLocalAmbiente(res, from, to) {
        var resultados = await hasLocalAmbiente.save({ _from: from, _to: to });
        res.status(200).json(resultados).end()

    }
    ambiente.list = function(req, res) {
        dbAmbiente.all()
            .then(cursor => {
                cursor.all()
                    .then(val => {
                        res.status(200).json(val).end()
                    })
            })
    };

    ambiente.lookup = function(req, res) {
        var id = req.params.id;
        dbAmbiente.document(id)
            .then(val => {
                res.status(200).json(val).end()
            }, err => {
                res.status(500).json(err).end()
            })
    };

    ambiente.edit = function(req, res) {
        var id = req.params.id;
        var data = req.body;
        var validate = Joi.validate(data, model);
        if (result.error != null) {
            res.status(400).json(validate.error);
        } else {
            dbAmbiente.update(id, data)
                .then(val => {
                    res.status(200).json(val).end()
                }, err => {
                    res.status(500).json(err).end()
                })
        }
    };

    ambiente.delete = function(req, res) {
        var id = req.params.id;
        dbAmbiente.remove(id)
            .then(val => {
                res.status(200).json(val).end()
            }, err => {
                res.status(500).json(err).end()
            })
    }

    return ambiente;
}
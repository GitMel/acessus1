module.exports = function (app) {
   var entity = app.controller.Entity;

   app.post("/entity",entity.new);
   app.get("/entity",entity.list);
   app.get("/entity/:id",entity.lookup);
   app.put("/entity/:id",entity.edit);
   app.delete("/entity/:id",entity.delete);
}

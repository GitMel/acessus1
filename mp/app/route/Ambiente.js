module.exports = function(app) {
    var ambiente = app.controller.Ambiente;

    app.post("/ambiente", ambiente.new);
    app.get("/ambiente", ambiente.list);
    app.get("/ambiente/:id", ambiente.lookup);
    app.put("/ambiente/:id", ambiente.edit);
    app.delete("/ambiente/:id", ambiente.delete);

}
module.exports = function (app) {
   var adaptacao = app.controller.Adaptacao;

   app.post("/adaptacao",adaptacao.new);
   app.get("/adaptacao",adaptacao.list);
   app.get("/adaptacao/:id",adaptacao.lookup);
   app.put("/adaptacao/:id",adaptacao.edit);
   app.delete("/adaptacao/:id",adaptacao.delete);
}

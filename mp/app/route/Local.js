module.exports = function (app) {
   var local = app.controller.Local;

   app.post("/local",local.new);
   app.get("/local",local.list);
   app.get("/local/:id",local.lookup);
   app.put("/local/:id",local.edit);
   app.delete("/local/:id",local.delete);
   //app.get("/local/geojson",local.newgeojson);
}

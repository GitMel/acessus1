module.exports = function (app) {
   var deficiencia = app.controller.Deficiencia;

   app.post("/deficiencia",deficiencia.new);
   app.get("/deficiencia",deficiencia.list);
}

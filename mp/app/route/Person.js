module.exports = function (app) {
   var person = app.controller.Person;

   app.post("/person",person.new);
   app.get("/person",person.list);
   app.get("/person/:id",person.lookup);
   app.put("/person/:id",person.edit);
   app.delete("/person/:id",person.delete);
}
